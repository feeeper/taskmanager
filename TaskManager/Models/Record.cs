﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaskManager.Models
{
    public class Record
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public RecordType Type { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public DateTime EndTime { get; set; }

        public string  Place { get; set; }

        public DTO.Record ToDto()
        {
            return new DTO.Record(this);
        }
    }

    public enum RecordType
    {
        Meeting,
        Task,
        Note
    }
}