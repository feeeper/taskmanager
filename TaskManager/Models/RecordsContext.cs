﻿using System;
using System.Data.Entity;

namespace TaskManager.Models
{
    public class RecordsContext : DbContext
    {
        public RecordsContext() : base("name=RecordsContext")
        {
            InitializeContext();
        }

        public RecordsContext(string connectionString) : base(connectionString)
        {
            InitializeContext();
        }

        private static void InitializeContext()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
            // Database.SetInitializer<RecordsContext>(new CreateDatabaseIfNotExists<RecordsContext>());
        }
        
        public DbSet<Record> Records { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Person> Persons { get; set; }
    }
}