﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskManager.Models;

namespace TaskManager.DTO
{
    public class Person
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime Birthday { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public IQueryable<Contact> Contacts { get; set; }

        public Person(Models.Person person)
        {
            Id = person.Id;
            LastName = person.LastName;
            FirstName = person.FirstName;
            Patronymic = person.Patronymic;
            Birthday = person.Birthday;
            Organization = person.Organization;
            Position = person.Position;
            Contacts = person.Contacts;
        }
    }
}