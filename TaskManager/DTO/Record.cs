﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using TaskManager.Models;

namespace TaskManager.DTO
{
    public class Record
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Subject { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Place { get; set; }

        public Record()
        {
        }

        public Record(Models.Record record)
        {
            Id = record.Id;
            Type = record.Type.ToString();
            Subject = record.Subject;
            StartTime = record.StartTime.ToString("O");
            EndTime = record.EndTime.ToString("O");
            Place = record.Place;
        }
    }
}