﻿namespace TaskManager.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public ContactType Type { get; set; }
        public string Value { get; set; }
        public virtual Person Person { get; set; }
    }

    public enum ContactType
    {
        Phone,
        Email,
        Skype,
        Other
    }
}