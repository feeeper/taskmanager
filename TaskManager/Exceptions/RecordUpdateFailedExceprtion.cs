﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManager.Exceptions
{
    public class RecordUpdateFailedExceprtion : Exception
    {
        public RecordUpdateFailedExceprtion(string message) : base(message)
        {
            
        }
    }
}