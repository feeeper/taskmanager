﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using TaskManager.Exceptions;
using TaskManager.Models;

namespace TaskManager.Controllers
{
    public class RecordsController : Controller
    {
        private RecordsContext _db = new RecordsContext();

        public RecordsController()
        {
        }

        public ActionResult Index()
        {
            var records = _db.Records.ToList().Select(x => x.ToDto()).ToList();
            return View(records);
        }
        
        public ActionResult Schedule()
        {
            var records = _db.Records.ToList().Select(x => x.ToDto()).ToList();
            return View(records);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(DTO.Record record)
        {
            try
            {
                var newRecord = _db.Records.Create();
                
                newRecord.Subject = record.Subject;
                DateTime result;
                if (DateTime.TryParseExact(record.StartTime, "yyyy/MM/dd HH:mm", new DateTimeFormatInfo(),
                    DateTimeStyles.None, out result))
                {
                    newRecord.StartTime = result;
                }
                else
                {
                    throw new RecordCreateFailedException("Дата не заполнена или имеет не верный формат");
                }

                if (DateTime.TryParseExact(record.EndTime, "yyyy/MM/dd HH:mm", new DateTimeFormatInfo(),
                    DateTimeStyles.None, out result))
                {
                    newRecord.EndTime = result;
                }
                else
                {
                    throw new RecordCreateFailedException("Дата не заполнена или имеет не верный формат");
                }

                RecordType type;
                if (Enum.TryParse(record.Type, true, out type))
                {
                    newRecord.Type = type;
                }
                else
                {
                    throw new RecordCreateFailedException("Тип записи не заполнен или имеет не верный формат");
                }

                _db.Records.Add(newRecord);

                _db.SaveChanges();
            }
            catch (Exception e)
            {
                ViewBag.Error = "Не удалось создать новую запись.";
                return View();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var record = _db.Records.Find(id).ToDto();

            if (record == null)
            {
                return HttpNotFound();
            }

            return View(record);
        }
        
        [HttpPost]
        public ActionResult Edit(DTO.Record record)
        {
            try
            {
                var updateableRecord = _db.Records.Find(record.Id);

                if (updateableRecord == null)
                {
                    return HttpNotFound();
                }

                updateableRecord.Subject = record.Subject;
                DateTime result;
                if (DateTime.TryParseExact(record.StartTime, "yyyy/MM/dd HH:mm", new DateTimeFormatInfo(),
                    DateTimeStyles.None, out result))
                {
                    updateableRecord.StartTime = result;
                }
                else
                {
                    throw new RecordUpdateFailedExceprtion("Дата не заполнена или имеет не верный формат");
                }

                if (DateTime.TryParseExact(record.EndTime, "yyyy/MM/dd HH:mm", new DateTimeFormatInfo(),
                    DateTimeStyles.None, out result))
                {
                    updateableRecord.EndTime = result;
                }
                else
                {
                    throw new RecordUpdateFailedExceprtion("Дата не заполнена или имеет не верный формат");
                }

                RecordType type;
                if (Enum.TryParse(record.Type, true, out type))
                {
                    updateableRecord.Type = type;
                }
                else
                {
                    throw new RecordUpdateFailedExceprtion("Тип записи не заполнен или имеет не верный формат");
                }
                
                _db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ViewBag.Error = "Обновление прощло с ошибкой";
                return View(record);
            }
        }
    }
}