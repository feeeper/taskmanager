﻿using System;
using System.Linq;

namespace TaskManager.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime Birthday { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public IQueryable<Contact> Contacts { get; set; }

        public DTO.Person ToDto()
        {
            return new DTO.Person(this);
        }
    }
}